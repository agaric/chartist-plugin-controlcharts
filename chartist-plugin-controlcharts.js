/*
 * Copyright (C) 2017, Agaric, AGPLv3 (see LICENSE)
 */
'use strict'

/* Simple sum of any array */
Array.prototype.sum = Array.prototype.sum || function() {
  return this.reduce(function(sum, num) { return sum + Number(num) }, 0);
}
/* Returns the average,
   if sample is true then average is of len-1 */
Array.prototype.average = Array.prototype.average || function(sample) {
  return this.sum() / ((this.length - ((sample||false)|0)) || 1);
}
/* Returns true if all values are the same */
Array.prototype.same = Array.prototype.same || function(sample) {
  if(this.length == 0) return false;
  return !this.reduce(function(diff, prev, num) {return diff || num != prev}, false, this[0]);
}
/* Returns the variance for this array,
   if sample is true returns sample variance */
Array.prototype.variance = Array.prototype.variance || function(sample) {
  var avg = this.average();
  return this.map(function(num) {return Math.pow(num - avg, 2) }).average(sample);
}
/* Returns the population deviation for this array,
   if sample is true, returns the sample deviation */
Array.prototype.deviation = Array.prototype.deviation || function(sample) {
  return Math.sqrt(this.variance(sample));
}

/* Just like Chartist.Interpolation.step but with steps
   half way between the pospone and non-pospone, this sent upstream
   for patching */
Chartist.Interpolation.halfstep = function(options) {
    var defaultOptions = {
      postpone: true,
      fillHoles: false
    };

    options = Chartist.extend({}, defaultOptions, options);

    return function step(pathCoordinates, valueData) {
      // Compatible postpone, false = 1, true = 0
      var shiftX = 1 - (options.postpone || 0);
      var path = new Chartist.Svg.Path();

      var prevX, prevY, thisX, prevData;

      for (var i = 0; i < pathCoordinates.length; i += 2) {
        var currX = pathCoordinates[i];
        var currY = pathCoordinates[i + 1];
        var currData = valueData[i / 2];

        // If the current point is also not a hole we can draw the step lines
        if(currData.value !== undefined) {
          if(prevData === undefined) {
            path.move(currX, currY, false, currData);
          } else {
            thisX = currX - (currX - prevX) * shiftX;
            // Line from the previous Y to the this X
            path.line(thisX, prevY, false, currData);
            // Line to the actual point (this should only be a Y-Axis movement
            path.line(thisX, currY, false, currData);
          }

          prevX = currX;
          prevY = currY;
          prevData = currData;
        } else if(!options.fillHoles) {
          prevX = prevY = prevData = undefined;
        }
      }

      if(prevData && thisX != prevX) {
        path.line(prevX, prevY, false, prevData);
      }

      return path;
    };
};

/* Notes about control charts

  http://www.npaihb.org/images/training_docs/NARCH/2010/Amin%20Control%20Charts%20101%20Quality%20Management%20in%20Health%20Care%202001.pdf

--== Variable data (i.e. measured in specific units like Celcius)  ==--
  (two parts, chart one is a moving range portion

These charts involve constants which involve calculation: https://andrewmilivojevich.com/xbar-and-r-chart/
and chart two is a process portion)

XmR-chart   - Variable data for single measurements
X&R-chart   - Variable data for average subgroups of no more than 10
X&S-chart   - Variable data for average subgroups of more than 10

--== Attributes data (i.e. actual numbers of units) ==--
 -- Numbers of --
  - Subgroups are the same size -
 * c-chart   - Number of non-conformances within one subgroup
  - Subgroups vary in size -
 * u-chart   - Number of Non-conformances per unit within one subgroup

 -- Counts of (i.e. %) --
  - Subgroups are the same size
 * np-chart  - Number nonconforming within one subgroup
  - Subgroups vary in size -
 * p-chart   - Fraction nonconforming within one subgroup

*/
var ControlChartTypes = {
  // Automatic (default) attempts to detect the best control chart to use
  // It's imperfect and shouldn't probably be used unless you want to figure
  // out what kind of chart you should be using and can follow the code.
  'auto': function(data, options) {
    var type_func = undefined;
    var type_name = undefined;
    Object.keys(ControlChartTypes).some(function(ptype) {
      if(Array.isArray(ControlChartTypes[ptype])) {
        var check = ControlChartTypes[ptype][1];
        if (typeof check === "function") {
          check = check(data, options);
        }
        if(check == true) {
          type_func = ControlChartTypes[ptype][0];
          type_name = ptype;
        }
        return check != true;
      }
    });
    if(type_name !== undefined) {
      console.warn("Auto type detected: " + type_name + ', set this in your chart setup.');
      return type_func(data, options);
    }
    console.error("Unable to auto detect control chart type, data provides no clues.");
  },
  'none': function(data, options) { return []; }
}

/* Wrapper function ControlChartType(...)
 *
 *   cc_name    - Name of this control chart.
 *   inner_call - function called on each set of data for each phase
 *   args       - list of argument names taken out of data.meta for use
 *                in the calculation function per phase.
 *   check_func - optional function which is called when the user specifies
 *                'auto' as the type of control chart. This function should
 *                attempt to identify if it applies to this chart or not.
 *                This is not expected to be exaustive and may warn on the
 *                console if it's unsure.
 }*/
function ControlChartType(cc_name, inner_call, args, check_func) {
  var process_func = function(data, options) {
    var phases = new Array();
    var output = new Array();
    var error = false;

    // There is always at least one phase.
    phases.push({});
    var op_phases = options.phases || data.meta.phases || new Array();

    // But there could be more if specified.
    if(op_phases == true) {
      // Detect and assume all phase information is layered in the data.
      for(var i = 0; i < data.meta[args[0]].length-1; i++) {
        // take the count of the first argument's list as the length.
        phases.push({});
      }
    } else if(Array.isArray(op_phases)) {
      for(var i = 0; i < op_phases.length; i++) {
        phases.push({});
      }
    } else {
      console.warn("Unknown phases option in console charts.");
    }

    // Check each of the args (scalars and arrays) in the data.meta
    for(var i = 0; i < args.length; i++) {
      var arg = data.meta[args[i]];
      // Special argument name 'series' for series data.
      if(arg === undefined && args[i] == 'series') {
        arg = data.series[0].data;
      }
      if(arg === undefined) {
        console.error(cc_name + ' needs meta data ' + args[i] + ' but it is missing.');
        error = true;
      }
      if(!op_phases || op_phases.length == 0) {
        // There aren't any phases, so populate one graph
        phases[0][args[i]] = arg;
      } else if(op_phases == true)  {
        // This means the data is already sliced up into arrays for us.
        for(var j = 0; j < arg.length; j++) {
          phases[j][args[i]] = arg[j];
        }
      } else if(Array.isArray(op_phases)){
        // This means we split at each of the points in phases
        if(arg.length == op_phases.length + 1) {
          // This is going to be a scalar, i.e the 'mean' value.
          for(var j = 0; j < arg.length; j++) {
            phases[j][args[i]] = arg[j];
          }
        } else {
          var so_far = 0;
          var new_phases = op_phases.slice();
          new_phases.push(arg.length);
          for(var j = 0; j < new_phases.length; j++) {
            phases[j][args[i]] = arg.slice(so_far, new_phases[j]);
            so_far = new_phases[j];
          }

        }
      }
    }

    // Don't continue if there was an error.
    if(error) { return; }

    for(var i = 0; i < phases.length; i++) {
      var ret = inner_call(phases[i], options);
      output.push({
        mean: ret[0],
        upperLimits: ret[1],
        lowerLimits: ret[2],
      });
    }
    return output;
  }
  ControlChartTypes[cc_name] = [process_func, check_func];
}

// Passthrough pushes data from the meta portion directly into
// the control chart display. Useful for pre-calculated UCL and LCL
ControlChartType('passthrough', function(data, options) {
  return [data.mean, data.ucl, data.lcl];
}, ['mean', 'ucl', 'lcl'], true);

// For subgroups vary in size, fraction nonconforming within one subgroup
ControlChartType('p-chart', function(data, options) {
  var process_avg = data.numerators.sum() / data.denominators.sum();
  var ucl = [];
  var lcl = [];
  for(var i = 0; i < data.numerators.length; i++) {
    var numerator = data.numerators[i];
    var denominator = data.denominators[i];
    var point = numerator / denominator;
    var sigma = Math.sqrt((process_avg * (1 - process_avg)) / denominator);
    ucl.push(process_avg + (3 * sigma));
    lcl.push(process_avg - (3 * sigma));
  }
  return [process_avg, ucl, lcl];
}, ['numerators', 'denominators'], false);


ControlChartType('u-chart', function(data, options) {
  var process_avg = data.numerators.sum() / data.denominators.sum();
  var ucl = [];
  var lcl = [];
  for(var i = 0; i < data.numerators.length; i++) {
    var numerator = data.numerators[i];
    var denominator = data.denominators[i];
    var point = numerator / denominator;
    var sigma = Math.sqrt((process_avg / denominator));
    ucl.push(process_avg + (3 * sigma));
    lcl.push(process_avg - (3 * sigma));
  }
  return [process_avg, ucl, lcl];
}, ['numerators', 'denominators'], false);


ControlChartType('c-chart', function(data, options) {
  // Counts only, not ratios
  var avg = data.series.sum() / (data.series.length * data.samples);
  var sigma = Math.sqrt(avg) * 3;
  return [avg, avg + sigma, avg - sigma];
}, ['series', 'samples'], false);


ControlChartType('s-chart', function(data, options) {
  // Counts only, not ratios
  var avg = data.series.sum() / (data.series.length * data.samples);
  var sigma = Math.sqrt(avg) * 3;
  return [avg, avg + sigma, avg - sigma];
}, ['series', 'samples'], false);


(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['chartist'], function (chartist) {
      return (root.returnExportsGlobal = factory(chartist));
    });
  } else if (typeof exports === 'object') {
    module.exports = factory(require('chartist'));
  } else {
    root['Chartist.plugins.controlcharts'] = factory(root.Chartist);
  }
}(this, function (Chartist) {
  /**
   * This Chartist plugin adds control charts layer to chartist line charts.
   */
  'use strict';

  var defaultOptions = {
    className: 'ct-control-chart',
    
    upperClassName: 'ct-cc-upper',
    lowerClassName: 'ct-cc-lower',
    meanClassName: 'ct-cc-mean',
    deviationClassName: 'ct-cc-area',
    gapClassName: 'ct-cc-gap',

    factor: 1,
    phases: null, // A list of indexes on the x-axis where a control phase seperator exists.
    lineSmooth: Chartist.Interpolation.halfstep({
      postpone: 0.5,
    }),
    gapSmooth: Chartist.Interpolation.none(),
  };

  Chartist.plugins = Chartist.plugins || {};

  Chartist.plugins.controlcharts = function (options) {
    var options = Chartist.extend({}, defaultOptions, options);
    // XXX We need a way to calculate the extent of the deviation so the bounds
    // can be updated with the right values.
    var type = options.type ? options.type : 'auto';
    type = typeof type === 'function' ? type : ControlChartTypes[type];
    if(Array.isArray(type)) { type = type[0]; } // Ignore check function and get process function
    if(typeof type !== 'function') { console.error("No control chart type '"+ options.type +"'."); return}

    return function controlcharts(chart) {

      if(chart.options.axisY.type === undefined) {
        chart.options.axisY.type = Chartist.ExtendedScaleAxis;
        // NOTE: We're abusing optionsProvider to update options.
        chart.optionsProvider = Chartist.optionsProvider(chart.options, chart.responsiveOptions, chart.eventEmitter);
      }

      chart.on('data', function (context) {
        context.data.controlChart = type(context.data, options);

	var highLow = { high: -Number.MAX_VALUE, low: Number.MAX_VALUE };

	// Function to recursively walk through arrays and find highest and lowest number
	function recursiveHighLow(data) {
	  if(data === undefined) {
	    return undefined;
	  } else if(Array.isArray(data)) {
	    for (var i = 0; i < data.length; i++) {
	      recursiveHighLow(data[i]);
	    }
	  } else if(Number.isFinite(data)) {
            var value = data * options.factor;
	    if (value > highLow.high) { highLow.high = value;}
	    if (value < highLow.low) { highLow.low = value;}
	  } else {
            Object.keys(data).forEach(function(key, index) {
	      recursiveHighLow(data[key]);
            });
          }
	}
        if(context.data.controlChart && context.data.controlChart.length > 0) {
          recursiveHighLow(context.data.controlChart);
          chart.options.axisY.extendHighLow = highLow;
          chart.optionsProvider = Chartist.optionsProvider(chart.options, chart.responsiveOptions, chart.eventEmitter);
        }
      });
      chart.on('created', function (context) {
        var rect = context.chartRect;
        var axisY = context.axisY;
        var axisX = context.axisX;
        function add_coord(coords, values, xValue, yValue) {
          xValue = xValue * options.factor;
          coords.push(
            rect.x1 + axisX.projectValue(xValue, yValue, null),
            rect.y1 - axisY.projectValue(xValue, yValue, null)
          );
          values.push({value: xValue, valueIndex: yValue, meta: {}});
        }

        // Make a container group
        var svg = context.svg.elem('g', undefined, undefined, true).addClass(options.className);

        var ccd = chart.data.controlChart;
        if(!ccd) {
          console.error("Control chart data was not calculated correctly.");
          return;
        }

        // For each of the shift indexes (usually just one for 'No shifts')
        var phase_index = 0;
        for(var phase = 0; phase < ccd.length; phase++) {
          if(!ccd[phase].upperLimits || !ccd[phase].lowerLimits || !ccd[phase].mean) {
            console.error("Phase " + phase + " didn't have upper/lower or mean.");
            break;
          }

          var op_phases = options.phases || chart.data.meta.phases;
          var phase_end = chart.data.series[0].data.length - 1;
          if(Array.isArray(ccd[phase].upperLimits)) {
            phase_end = phase_index + ccd[phase].upperLimits.length - 1;
          } else if(Array.isArray(op_phases) && op_phases.length > phase) {
            phase_end = op_phases[phase];
          }
          var mean = ccd[phase].mean * options.factor;
          var meanY = rect.y1 - axisY.projectValue(mean, phase_index, null);
          svg.elem('line', {
            x1: rect.x1 + axisX.projectValue(mean, phase_index, null),
            x2: rect.x1 + axisX.projectValue(mean, phase_end, null),
            y1: meanY, y2: meanY,
          }, options.meanClassName);

          if(Array.isArray(ccd[phase].lowerLimits)) {
            // Control chart is varient (jagged upper and lower control limits)
            var upperValues = [],
                lowerValues = [],
                upperCoords = [],
                lowerCoords = [];

            if(phase > 0 && options.gapSmooth) {
              // Create a join between the two phases moving clockwise
              var gapCoords = [];
              var gapValues = [];
              var last_end = ccd[phase-1].upperLimits.length - 1;
              add_coord(gapCoords, gapValues, ccd[phase-1].upperLimits[last_end], phase_index - 1);
              add_coord(gapCoords, gapValues, ccd[phase].upperLimits[0], phase_index);
              add_coord(gapCoords, gapValues, ccd[phase].lowerLimits[0], phase_index);
              add_coord(gapCoords, gapValues, ccd[phase-1].lowerLimits[last_end], phase_index - 1);
              var gapPath = options.gapSmooth(gapCoords, gapValues);
              var path = svg.elem('path', {d: gapPath.stringify()}, options.gapClassName);
            }

            // Generate the upper and lower coordinates for each varient position
            for(var i=0; i<ccd[phase].upperLimits.length; i++) {
              add_coord(upperCoords, upperValues, ccd[phase].upperLimits[i], phase_index);
              add_coord(lowerCoords, lowerValues, ccd[phase].lowerLimits[i], phase_index);
              phase_index += 1;
            }

            // Generate uper and lower lines from the coordinates.
            var upperPath = options.lineSmooth(upperCoords, upperValues);
            var lowerPath = options.lineSmooth(lowerCoords, lowerValues);
            svg.elem('path', {d: upperPath.stringify()}, options.upperClassName);
            svg.elem('path', {d: lowerPath.stringify()}, options.lowerClassName);

            /*
             * The path is really just the lower line plus the upper line
             * in order, this can be done generically no matter what the
             * calculation or type of chart.
             */
            var area = new Chartist.Svg.Path(true, undefined);
            // First add the upper path, left-to-right order
            for(var j = 0; j < upperPath.pathElements.length; j++) {
              area.pathElements.push(upperPath.pathElements[j]);
            }
            // Next add the lower path [BACKWARDS], right-to-left order
            for(var j = lowerPath.pathElements.length-1; j >= 0; j--) {
               area.pathElements.push(lowerPath.pathElements[j]);
               if(j==0) {
                 // Right at the end, the final instruction is LINE (not MOVE)
                 area.pathElements[area.pathElements.length-1].command = "L";
               }
            }
            svg.elem('path', {d: area.stringify()}, options.deviationClassName, true);
          } else {
            // Two lines and a single box for simplified (non-varient) display.
            var x1 = rect.x1 + axisX.projectValue(mean, phase_index, null);
            var x2 = rect.x1 + axisX.projectValue(mean, phase_end, null);
            var upperY = rect.y1 - axisY.projectValue(ccd[phase].upperLimits * options.factor);
            svg.elem('line', {
                x1: x1, x2: x2,
                y1: upperY, y2: upperY,
            }, options.upperClassName);

            var lowerY = rect.y1 - axisY.projectValue(ccd[phase].lowerLimits * options.factor);
            svg.elem('line', {
                x1: x1, x2: x2,
                y1: lowerY, y2: lowerY,
            }, options.lowerClassName);

            svg.elem('rect', {
                x: x1, width: x2 - x1,
                y: upperY, height: lowerY - upperY,
            }, options.deviationClassName, true)

            phase_index = phase_end;
          }
        }
      });
    };

  };
  return Chartist.plugins.controlcharts;
}));

/*
 * Chartist extended scale provides ability to add to the automatic scale
 * for non-data related points.
 */
(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['chartist'], function (chartist) {
      return (root.returnExportsGlobal = factory(chartist));
    }); 
  } else if (typeof exports === 'object') {
    module.exports = factory(require('chartist'));
  } else {
    root['Chartist.ExtendedScaleAxis'] = factory(root.Chartist);
  }
}(this, function (Chartist) {
  'use strict';

  function ExtendedScaleAxis(axisUnit, data, chartRect, options) {
    var highLow = Chartist.getHighLow(data, options, axisUnit.pos);
    if(highLow && options.extendHighLow) {
      if(highLow.high < options.extendHighLow.high) {
        highLow.high = options.extendHighLow.high;
      }
      if(highLow.low > options.extendHighLow.low) {
        highLow.low = options.extendHighLow.low;
      }
      options.highLow = highLow;
    }

    return Chartist.ExtendedScaleAxis.super.constructor.call(this,
      axisUnit, data, chartRect, options);
  }

  Chartist.ExtendedScaleAxis = Chartist.AutoScaleAxis.extend({
    constructor: ExtendedScaleAxis,
  });

  return Chartist.ExtendedScaleAxis;
}));

