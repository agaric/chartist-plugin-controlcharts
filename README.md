# chartist-plugin-control

A control chart plugin for [Chartist](https://github.com/gionkunz/chartist-js)
charts.

Control charts allow you to see graphically if points are outside of the
standard diviation for the series. There are a number of types, so pay close
attention to which one you end up using. It's assumed you have a passing
knowlege of statistics.

![Example Chart](example.png)

## Installation

Download the js file. We're not using anything fancy here.

## Using the Library

The `index.html` file contains a number of demonstrations which should act as
an example of how to use the different types of control chart.

Quick example:

```
  new Chartist.Line('.ct-cc-example', {
   labels: ['1', '2', '3', '4', '5', '6', '7'],
   series: [
       { "name": "Line", "data": [6.3, 6.2, 6.8, 6.2, 4, 6, 10] },
   ],
   meta: {
     "numerators":   [44, 32, 44, 49, 45, 48, 44],
     "denominators": [62, 37, 60, 68, 52, 61, 60],
   }, {
    plugins: [
      Chartist.plugins.controlcharts({type: 'c-chart'}),
      ...
    ]
  });
```

## Custom Control Chart Type

We welcome contributors to this project, epsecially since it only supports a
limited number of chart types.

To make a new control chart type, create a function like the following:

```
ControlChartType(name, calculation_function, meta_arguments, check_function);
```

The `name` is a simple string for use in the chart plugin creation. Such as
the existing `p-chart` or `u-chart` names.

The `calculate_function` is used to generate the mean value, the upper control line
and the lower control lines. In this nonsense exmple, we return the average plus
and minus 3. The function should always return three values, but the upper and
lower control line variables may be lists of values if the control lines are
variable instead of strait lines.

```
function calculation_function(data, options) {
  var avg = data.numerators.sum() / data.denominators.sum();
  return [avg, avg + 3, avg - 3];
}
```

The `meta_arguments` is a list of strings which will be required in the meta
data of the chart. For example in the above function we need numerator
and denominator so specify ['numerator', 'denominator']. Values will be
automatically parsed out of the meta data if there are phases or data
which needs to be unpacked before the `calculation_function` can run.

The `check_function` is used to automatically detect if a chart should be used.
If it returns true, this chart is appropriate for the data in this chart.
This function is currently experimental and isn't well tested.

# Contributing

This project is open source. Please create a merge request if you would like
to contribute a control chart type or fix a bug. The license is AGPLv3,
copyright their respective owners.

And thanks!

